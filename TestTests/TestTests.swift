//
//  TestTests.swift
//  TestTests
//
//  Created by Christos Bountalis on 26/10/20.
//

import XCTest

class TestTests: XCTestCase {
    func testHelperUtility() throws {
        XCTAssertTrue(HelperUtility().calculate(.always))
        XCTAssertTrue(HelperUtility().calculate(.sometimes))
        XCTAssertFalse(HelperUtility().calculate(.never))
    }
}
