import UIKit

internal struct HelperUtility {
    enum SampleEnum {
        case always
        case sometimes
        case never
    }
    
    internal func calculate(_ value: SampleEnum) -> Bool {
        switch value {
        case .always:
            return true
        case .sometimes:
            return true
        case .never:
            return false
        }
    }
}
