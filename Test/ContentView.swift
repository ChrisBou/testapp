//
//  ContentView.swift
//  Test
//
//  Created by Christos Bountalis on 21/10/20.
//

import SwiftUI

struct ContentView: View {
    
    let helper = HelperUtility()
    
    var body: some View {
        VStack {
            Text("Hello, world!")
                .padding()
            Text("value is: \(helper.calculate( .always).description)")
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
